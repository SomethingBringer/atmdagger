package com.example.atmdaggerexample.Utils

//TODO 1: Now we will learn how to provide some method using Dagger. Create new interface
interface Outputter {
    fun output(output: String)
}