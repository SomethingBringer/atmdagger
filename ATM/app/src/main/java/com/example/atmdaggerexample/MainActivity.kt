package com.example.atmdaggerexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.atmdaggerexample.Utils.CommandRouter
import com.example.atmdaggerexample.Utils.CommandRouterFactory
import com.example.atmdaggerexample.Utils.DaggerCommandRouterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val terminalWindow = findViewById<EditText>(R.id.terminalWindow)
        val responseWindow = findViewById<TextView>(R.id.responseWindow)
        val workBtn = findViewById<Button>(R.id.workBtn)
        val commandRouterFactory = DaggerCommandRouterFactory.create()
        val commandRouter = commandRouterFactory.router()

        workBtn.setOnClickListener{commandRouter.route(terminalWindow.text.toString())
        responseWindow.text = commandRouter.msg}
    }
}
