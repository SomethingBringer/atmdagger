package com.example.atmdaggerexample.Utils

import dagger.Module
import dagger.Provides

//TODO 3: Module
@Module
abstract class SystemOutModule {
    //TODO 4: Annotate fun with Provides. Its like Inject, but for functions.
    @Provides
    fun textOutputter(): Outputter{
        return System.out::println
    }//Here is a mistake and i dont know what to do with it. 
}