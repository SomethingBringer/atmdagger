package com.example.atmdaggerexample.Utils

import dagger.Binds
import dagger.Module
@Module
abstract class HelloWorldModule {
    @Binds
    abstract fun helloWorldCommand(command: HelloWorldCommand): Command
}