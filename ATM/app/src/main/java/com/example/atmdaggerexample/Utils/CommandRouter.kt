package com.example.atmdaggerexample.Utils

import java.util.*
import javax.inject.Inject
import com.example.atmdaggerexample.Utils.Command

class CommandRouter {
    private var commands: MutableMap<String,Command> = Collections.emptyMap()
    var msg =""
    @Inject constructor(command: Command){
        commands.put(command.key(),command)
    }

    fun route(input: String):Command.Status{
        var splitInput: List<String> = input.split(' ')
        if(splitInput.isEmpty()){ return invalidCommand(input)}
        var commandKey=splitInput.get(0)
        var command = commands.get(commandKey)
        if(command==null){ return  invalidCommand(input)}
        var status = command.handleInput(splitInput.subList(1,splitInput.size))
        if (status==Command.Status.INVALID){msg = commandKey+ ": invalid arguments"}
        return status
    }
    private fun invalidCommand(input: String):Command.Status{
        msg = String.format("couldn't understand \"%s\". please try again.", input)
        return Command.Status.INVALID
    }
}
