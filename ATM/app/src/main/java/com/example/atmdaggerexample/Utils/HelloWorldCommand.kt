package com.example.atmdaggerexample.Utils

import javax.inject.Inject
//TODO 2: Create outputter field in our class and change output method
class HelloWorldCommand @Inject constructor(val outputter: Outputter) : Command{


    override fun key(): String {
        return "Hello"
    }

    override fun handleInput(input: List<String>): Command.Status {
        if (!input.isEmpty()) {
            return Command.Status.INVALID
        }
        outputter.output("world!")
        return Command.Status.HANDLED
    }
}